# Variables
image_name=slo_luanti_server
image_tag=latest
container_name=slo_luanti_server

# Target who are not file name
.PHONY: build run-locally stop-locally


# Targets
all: build

build:
	@echo "Build the image"
	podman build -t "$(image_name):$(image_tag)" .

run-locally:
	@echo "Run locally the container (not for production)"
	podman run --rm -p 30000:30000/udp --name "$(container_name)" "localhost/$(image_name):$(image_tag)"

stop-locally:
	@echo "Stop the container run locally"
	podman stop "$(container_name)"
