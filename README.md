# Luanti server of Swisslinux.org #

Here, you can found the code of our Luanti server.

This server use Luanti and VoxeLibre


## About Luanti ##

An open source voxel game engine. Play one of our many games, mod a
game to your liking, make your own game, or play on a multiplayer
server.

Available for Windows, macOS, GNU/Linux, FreeBSD, OpenBSD, DragonFly
BSD, and Android.

[Website](https://www.minetest.net/)
[Wiki of Luanti](https://wiki.minetest.net)


## About VoxeLibre ##

A game inspired by Minecraft for Luanti. Forked from MineClone by
davedevils.

[Source code](https://git.minetest.land/VoxeLibre/VoxeLibre)


## About Security ##

This image is made to be run with `podman` in non-root mode.


## How to build this server ##


You need `podman` to build a container image of our server.

To build, run this commande:
``` sh
make build
```


## How to run locally ##

Example of command to run the server:
``` sh
make run-locally
```

### Volumes ###

List of volumes:
- `/var/lib/minetest/.minetest/debug.txt`: Log file
- `/var/lib/minetest/.minetest/worlds/`: Directory where are the worlds are, you need to backup it


### Config ###

See the `minetest.conf` file to see the configuration.


## Licences ##

- Luanti: LGPLv2.1
- VoxeLibre: GPLv3
- Files of this repo (submodules not included): GPLv3


## Author ##

Sébastien Gendre <seb@k-7.ch>

